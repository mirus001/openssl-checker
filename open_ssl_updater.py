import SoftLayer
import subprocess
import paramiko
import traceback

DATACENTER = 'tor01'
SHOW_VERSION_COMMAND = "openssl version -a"
CREDENTIALS = { "geminiops": "/Users/mirus/.ssh/geminiops.pem", "smadevops": "/Users/mirus/.ssh/smadevops.pem"}
CENTOS_OPEN_SSL_CHECK = "rpm -qa | grep openssl"
DEBIAN_OPEN_SSL_CHECK = "dpkg -l | grep openssl"
def retrieve_bm_credentials(datacenter, device_id):
	try:
		result = (subprocess.check_output(["slcli hardware detail "+str(device_id)+" --passwords | grep users"], shell=True)).split('\n')[0:-1]
		
		i=0
		for credential in result:
			result[i] = credential.split("users",1)[1].split()
			i=i+1
		return result
	except:
		return None

def retrieve_vs_credentials(datacenter, device_id):
	try:
		result = (subprocess.check_output(["slcli vs detail "+str(device_id)+" --passwords | grep users"], shell=True)).split('\n')[0:-1]

		i=0
		for credential in result:
			result[i] = credential.split("users",1)[1].split()
			i=i+1
		return result
	except:
		return None

def get_baremetal_device_list(datacenter):
	client = SoftLayer.Client()
	mgr = SoftLayer.HardwareManager(client)
	return mgr.list_hardware(datacenter=datacenter)

def get_virtual_server_list(datacenter):
	client = SoftLayer.Client()
	mgr = SoftLayer.VSManager(client)
	return mgr.list_instances(datacenter=DATACENTER)

def get_vulnerable_packages(drown_output):
	result = []
	splitty = drown_output.split("\n")
	for row in splitty:
		if "WARNING" in row:
			package = row.split("The installed version of", 1)[1]
			package = package.split('(',1)[0].strip()
			print package
			result.append(package)
	return result

def list_vulnerable_devices():
	client = SoftLayer.Client()
	
	bm = get_baremetal_device_list(DATACENTER)
	vs =  get_virtual_server_list(DATACENTER)
	device_list = bm + vs
	
	log_file = open('logfile.txt', 'w')
	report_file = open('openssl_report.txt', 'w')
	SHOW_VERSION_COMMAND = "openssl version -a"

	for device in device_list:
		if "prod" in str(device['fullyQualifiedDomainName']):
			print "***"
			print device['id'], ":", device['primaryBackendIpAddress'], ":", device['fullyQualifiedDomainName']

			credential_set = retrieve_bm_credentials(DATACENTER, device['id'])
			if credential_set is None:
				credential_set = retrieve_vs_credentials(DATACENTER, device['id'])
			print credential_set
			login_with_key = False
			login_with_sl_creds = False

			cred_count = 0
			while login_with_key is False and cred_count < len(CREDENTIALS):
				try:
					uname = CREDENTIALS.keys()[cred_count]
					keyfile = CREDENTIALS.values()[cred_count]
					print "attempting to connect to", str(device['primaryBackendIpAddress']), " (",str(device['fullyQualifiedDomainName']),") with", uname, ":", keyfile
					key = paramiko.RSAKey.from_private_key_file(keyfile)
					ssh = paramiko.SSHClient()
					ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
					ssh.connect(str(device['primaryBackendIpAddress']), 22, username=uname, pkey=key, timeout=2)
					print "Connection successful!"
					login_with_key = True			
				except Exception as e:	
					#traceback.print_exc()
					print "could not connect via provided public key for user "+uname
				finally:
					cred_count = cred_count+1

			if not login_with_key:
				
				for cred in credential_set:
					if not login_with_sl_creds:
						try:
							print "Attempting to connect with", cred[0], ":", cred[1]
							ssh = paramiko.SSHClient()
							ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
							ssh.connect(device['primaryBackendIpAddress'], username=cred[0], password=cred[1], timeout=2)
							print "Connection Successful!"
							login_with_sl_creds = True
							#log_file.write("Connected to "+str(device['primaryBackendIpAddress'])+" ("+str(device['fullyQualifiedDomainName'])+") with "+cred[0]+":"+cred[1]+"\n")
						except Exception as e:
							print "could not connect via SL provided credentials"
							

				if not login_with_sl_creds:
					log_file.write("!!!Could NOT connect to "+str(device['primaryBackendIpAddress'])+" ("+str(device['fullyQualifiedDomainName'])+") with "+str(credential_set)+" or pem file \n")

			if login_with_sl_creds or login_with_key:
				log_file.write("\nConnected to "+str(device['primaryBackendIpAddress'])+" ("+str(device['fullyQualifiedDomainName'])+") \n")
				# Check kernel version
				stdin, stdout, stderr = ssh.exec_command('uname -r')
				exit_status = stdout.channel.recv_exit_status()          # Blocking call
				output = stdout.read()
				report_file.write("Connected to: "+str(device['primaryBackendIpAddress'])+" ("+str(device['fullyQualifiedDomainName'])+") \n")
				kernel_version = str(output.split())
				print("Kernel version: "+kernel_version)
				report_file.write("\tKernel version: "+kernel_version+"\n")

				# Check platform version
				stdin, stdout, stderr = ssh.exec_command('python -mplatform')
				exit_status = stdout.channel.recv_exit_status()          # Blocking call
				output = stdout.read()
				is_centos = "centos" in output or "redhat" in output
				is_debian = "debian" in output or "ubuntu" in output
					 
				print("Platform: "+output)
				report_file.write("\tPlatform: "+output+"\n")

				# Check openssl version
				if is_centos:
					SHOW_VERSION_COMMAND = CENTOS_OPEN_SSL_CHECK
				else:
					SHOW_VERSION_COMMAND = DEBIAN_OPEN_SSL_CHECK

				stdin, stdout, stderr = ssh.exec_command(SHOW_VERSION_COMMAND)
				exit_status = stdout.channel.recv_exit_status()          # Blocking call
				output = stdout.read()
				openssl_version = output.split('\n')
				print("Installed version: "+str(openssl_version[0]))
				report_file.write("\tInstalled openssl version: "+str(openssl_version[0])+"\n")
				sftp = ssh.open_sftp()
				sftp.put('./DROWN-test.sh', 'DROWN-test.sh')

				stdin, stdout, stderr = ssh.exec_command('bash DROWN-test.sh')
				exit_status = stdout.channel.recv_exit_status()          # Blocking call
				output = stdout.read()
				packages_to_upgrade = get_vulnerable_packages(output)
				print output
				if len(packages_to_upgrade) > 0:
					print "Packages to upgrade:", packages_to_upgrade

				# run update on openssl
				if len(packages_to_upgrade) > 0:
					stdin, stdout, stderr = ssh.exec_command('sudo -s yum check-update -y openssl')
					exit_status = stdout.channel.recv_exit_status()          # Blocking call
					output = stdout.read()
					report_file.write("\tAvailable Version: "+output+"\n")

				ssh.exec_command('rm -f DROWN-test.sh')
				report_file.write(output+"\n")
				stdin, stdout, stderr = ssh.exec_command("sudo -S ")
				exit_status = stdout.channel.recv_exit_status()          # Blocking call
				output = stdout.read().split('\n')
				if ssh:
					ssh.close()

list_vulnerable_devices()
#print get_baremetal_device_list("tor01")

#for key, value in CREDENTIALS.iteritems():
#	print key, value